package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"time"
)

func main() {
	arguments := os.Args
	if len(arguments) == 1 {
		fmt.Println("Please provide host:port.")
		return
	}

	CONNECT := arguments[1]
	fmt.Printf("Connecting to '%s'\n", CONNECT)
	c, err := net.Dial("tcp", CONNECT)
	if err != nil {
		fmt.Println(err)
		return
	}
	counter := 0
	for {
		text := fmt.Sprintf("What time is it? [%d]\n", counter)
		fmt.Fprintf(c, text)
		message, err := bufio.NewReader(c).ReadString('\n')
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Printf("[%d]->: %s\n", counter, message)
		counter++
		time.Sleep(2 * time.Second)
	}
}
